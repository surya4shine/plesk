@extends('admin.layout.base')

@section('title', 'Provider Documents ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Provider Service Type Allocation</h5>
            <div class="row">
                <div class="col-xs-12">
                    @if($ProviderService->count() > 0)
                    <hr><h6>Allocated Services :  </h6>
                    <table class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>Provider Name</th>
                                <th>Service Name</th>
                                <th>Service Number</th>
                                <th>Service Model</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ProviderService as $service)
                            <tr>
                                <td>{{ $service->provider->first_name }}</td>
                                <td>{{ $service->service_type->name }}</td>
                                <td>{{ $service->service_number }}</td>
                                <td>{{ $service->service_model }}</td>
                                <td>
                                    
                                    <form action="{{ route('admin.provider.fleetservice.service',[$service->provider_id,$service->id])}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-large btn-block">Delete</a>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Provider Name</th>
                                <th>Service Name</th>
                                <th>Service Number</th>
                                <th>Service Model</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    @endif
                    <hr>
                </div>
                <form action="{{ route('admin.provider.fleetservice.store')}}?fleet={{$fleet}}&serviceid={{$serviceid}}" method="POST">
                <input type="hidden" value="{{ $FleetService->service_type_id }}" name="service_type_id" />
                <input type="hidden" value="{{ $FleetService->id }}" name="fleet_service_id" />
                <input type="hidden" value="{{ $FleetService->service_number }}" name="service_number" />
                <input type="hidden" value="{{ $FleetService->service_model }}" name="service_model" />

                    {{ csrf_field() }}
                    <div class="col-xs-3">
                        <select class="form-control input" name="provider_id" required>
                            @forelse($Provider as $key => $val)
                            <option value="{{ $key }}">{{ $val }}</option>
                            @empty
                            <option value="0">- Please Create a Provider first -</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <button class="btn btn-primary btn-block" type="submit">Update</button>
                    </div>
                </form>






                
            </div>
        </div>

        <div class="box box-block bg-white">
            <h5 class="mb-1">Provider Documents</h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection