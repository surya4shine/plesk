@extends('admin.layout.base')

@section('title', 'Provider Documents ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">{{$FleetService->service_model}} document upload</h5>
            <div class="row">
<style>
.docuploadtbl p{
    margin-bottom: 0px;
}
.docuploadtbl{
    width: 98%;
    margin: 0 auto;
}
</style>
<table class="table table-striped table-bordered dataTable docuploadtbl">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Name</th>
                        <th>Status</th>
                        <th>Upload</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ServiceDocuments as $Index => $Document)
                    <tr>
                        <td>{{ $Index + 1 }}</td>
                        <td>
                            <p class="manage-txt">{{ $Document->name }}</p>
                        </td>
                        <td>
                            <p class="manage-badge {{ $FleetService->document($Document->id) ? ($FleetService->document($Document->id)->status == 'ASSESSING' ? 'yellow-badge' : 'green-badge') : 'red-badge'}}">
                                        {{ $FleetService->document($Document->id) ? $FleetService->document($Document->id)->status : 'MISSING' }}
                            </p>
                        </td>
                        <td style="width:40%">
                               
                                <form action="{{ route('admin.provider.documents.update', $Document->id) }}" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }}
                                            
                                            
                                                <textarea name="fleet_service_id" style="display:none">{{$FleetService->id}}</textarea>
                                                <textarea name="document_type" style="display:none">VEHICLE</textarea>
                                                <input type="file" name="document" accept="application/pdf, image/*">
                                            <span class="btn-submit">
                                                <button class="btn btn-success btn-large">Upload</button>
                                            </span>
                                            
                                        </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
</table>
        <div class="box box-block bg-white">
            <h5 class="mb-1">Service Documents</h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($FleetService->documents as $Index => $Document)
                    <tr>
                        <td>{{ $Index + 1 }}</td>
                        <td>{{ $Document->document->name }}</td>
                        <td>{{ $Document->status }}</td>
                        <td>
                            <div class="input-group-btn">
                                <a href="{{ route('admin.provider.servicedocument.edit', [$FleetService->id, $Document->id]) }}"><span class="btn btn-success btn-large">View</span></a>
                                <button class="btn btn-danger btn-large" form="form-delete">Delete</button>
                                <form action="{{ route('admin.provider.servicedocument.destroy', [$FleetService->id, $Document->id]) }}" method="POST" id="form-delete">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection