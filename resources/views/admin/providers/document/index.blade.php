@extends('admin.layout.base')

@section('title', 'Provider Documents ')

@section('content')
<style>
.docuploadtbl p{
    margin-bottom: 0px;
}
.docuploadtbl{
    width: 98%;
    margin: 0 auto;
}

</style>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">{{$Provider->first_name}} document upload</h5>
            <div class="row">

<table class="table table-striped table-bordered dataTable docuploadtbl">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Name</th>
                        <th>Status</th>
                        <th>Upload</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($DriverDocuments as $Index => $Document)
                    <tr>
                        <td>{{ $Index + 1 }}</td>
                        <td>
                            <p class="manage-txt">{{ $Document->name }}</p>
                            <p class="license">Expires: {{ $Provider->document($Document->id) ? $Provider->document($Document->id)->expires_at : 'N/A' }}</p>
                        </td>
                        <td>
                            <p class="manage-badge {{ $Provider->document($Document->id) ? ($Provider->document($Document->id)->status == 'ASSESSING' ? 'yellow-badge' : 'green-badge') : 'red-badge'}}">
                                {{ $Provider->document($Document->id) ? $Provider->document($Document->id)->status : 'MISSING' }}
                            </p>
                        </td>
                        <td style="width:40%">
                               
                                <form action="{{ route('admin.provider.documents.update', $Document->id) }}" method="POST" enctype="multipart/form-data" id="form-upload">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }}
                                            
                                            
                                                <textarea name="provider_id" style="display:none">{{$Provider->id}}</textarea>
                                                <textarea name="document_type" style="display:none">DRIVER</textarea>
                                                <input type="file" name="document" accept="application/pdf, image/*">
                                            <span class="btn-submit">
                                                <button class="btn btn-success btn-large">Upload</button>
                                            </span>
                                            
                                        </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
</table>
        <div class="box box-block bg-white">
            <h5 class="mb-1">Provider Documents</h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Provider->documents as $Index => $Document)
                    <tr>
                        <td>{{ $Index + 1 }}</td>
                        <td>{{ $Document->document->name }}</td>
                        <td>{{ $Document->status }}</td>
                        <td>
                            <div class="input-group-btn">
                                <a href="{{ route('admin.provider.prodocument.edit', [$Provider->id, $Document->id]) }}"><span class="btn btn-success btn-large">View</span></a>
                                <button class="btn btn-danger btn-large" form="form-delete">Delete</button>
                                <form action="{{ route('admin.provider.prodocument.destroy', [$Provider->id, $Document->id]) }}" method="POST" id="form-delete">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection