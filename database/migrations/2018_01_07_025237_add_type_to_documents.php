<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            DB::statement("ALTER TABLE documents MODIFY type ENUM('DRIVER', 'VEHICLE', 'OWNER') NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
              DB::statement("ALTER TABLE documents MODIFY type ENUM('DRIVER', 'VEHICLE' ) NOT NULL");
        });
    }
}
