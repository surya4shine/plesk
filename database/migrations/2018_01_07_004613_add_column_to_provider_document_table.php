<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToProviderDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provider_documents', function (Blueprint $table) {
            $table->string('document_type')->after('document_id');
            $table->integer('fleet_id')->after('document_type')->default(0);
            $table->integer('fleet_service_id')->after('fleet_id');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provider_documents', function (Blueprint $table) {
            $table->dropColumn('document_type');
            $table->dropColumn('fleet_id');
            $table->dropColumn('fleet_service_id');
          
        });
    }
}
