<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyFleetServiceIdColumnInProviderDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provider_documents', function (Blueprint $table) {
            $table->dropColumn('fleet_service_id');
        });
         Schema::table('provider_documents', function (Blueprint $table) {
            $table->integer('fleet_service_id')->after('fleet_id')->default(0);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provider_documents', function (Blueprint $table) {
            $table->dropColumn('fleet_service_id');
        });
         Schema::table('provider_documents', function (Blueprint $table) {
            $table->integer('fleet_service_id')->after('fleet_id');
           
        });
    }
}
