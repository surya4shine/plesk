<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('mobile');
            $table->dropColumn('email');
        });

        Schema::table('providers', function (Blueprint $table) {
            $table->string('password')->nullable();
            $table->string('mobile')->unique();
            $table->string('email')->nullable();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('mobile');
            $table->dropColumn('email');
        });

        Schema::table('providers', function (Blueprint $table) {
            $table->string('password');
            $table->string('mobile')->nullable();
            $table->string('email')->unique();
        });
    }
}
