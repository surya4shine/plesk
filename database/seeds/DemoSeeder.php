<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards')->truncate();
        DB::table('promocodes')->truncate();
        DB::table('promocode_usages')->truncate();
        DB::table('provider_devices')->truncate();
        DB::table('provider_documents')->truncate();
        DB::table('provider_profiles')->truncate();
        DB::table('provider_services')->truncate();
        DB::table('request_filters')->truncate();
        DB::table('user_request_payments')->truncate();
        DB::table('user_request_ratings')->truncate();
        DB::table('user_requests')->truncate();
        DB::table('users')->truncate();
        DB::table('users')->insert([[
            'first_name' => 'Appoets',
            'last_name' => 'Demo',
            'email' => 'demo@appoets.com',
            'password' => bcrypt('123456'),
            'mobile' => '9258632148',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'picture' => 'http://lorempixel.com/512/512/business/Tranxit',
        ],[
            'first_name' => 'Emilia',
            'last_name' => 'Epps',
            'email' => 'emilia@appoets.com',
            'password' => bcrypt('123456'),
            'mobile' => '9258632856',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'picture' => 'http://lorempixel.com/512/512/business/Tranxit',
        ]]);

        DB::table('providers')->truncate();
        DB::table('providers')->insert([[
            'first_name' => 'Appoets',
            'last_name' => 'Demo',
            'email' => 'demo@appoets.com',
            'password' => bcrypt('123456'),
            'mobile' => '8465562222',
            'status' => 'approved',
            'latitude' => '13.00',
            'longitude' => '80.00',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'avatar' => 'http://lorempixel.com/512/512/business/Tranxit',
        ],[
            'first_name' => 'Thomas',
            'last_name' => 'Jenkins',
            'email' => 'thomas@appoets.com',
            'password' => bcrypt('123456'),
            'mobile' => '8697730561',
            'status' => 'approved',
            'latitude' => '13.00',
            'longitude' => '80.00',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'avatar' => 'http://lorempixel.com/512/512/business/Tranxit',
        ]]);

        DB::table('provider_services')->truncate();
        DB::table('provider_services')->insert([[
            'provider_id' => 1,
            'service_type_id' => 1,
            'status' => 'active',
            'service_number' => '4ppo3ts',
            'service_model' => 'Audi R8',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'provider_id' => 2,
            'service_type_id' => 1,
            'status' => 'active',
            'service_number' => '4ppo3ts',
            'service_model' => 'Audi R8',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]]);
    }
}