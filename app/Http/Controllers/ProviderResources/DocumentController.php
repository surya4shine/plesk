<?php

namespace App\Http\Controllers\ProviderResources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Document;
use App\ProviderDocument;
use App\Fleet;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VehicleDocuments = Document::vehicle()->get();
        $DriverDocuments = Document::driver()->get();

        $Provider = \Auth::guard('provider')->user();

        //echo '<pre>';
        //print_r($DriverDocuments);exit;

        return view('provider.document.index', compact('DriverDocuments', 'VehicleDocuments','Provider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'document' => 'mimes:jpg,jpeg,png,pdf',
            ]);

        if(!is_object ($request->document)){
            return back()->with('flash_error', 'Invalid document uploaded!');
        }

        if(isset($request->provider_id)){
            $provider_id = $request->provider_id;
        }else{
            $provider_id = 0;
        }

        if(isset($request->fleet_service_id)){
            $fleet_service_id = $request->fleet_service_id;
        }else{
            $fleet_service_id = 0;
        }

        if(isset($request->document_type)){
            $document_type = $request->document_type;
        }else{
            $document_type = 'DRIVER';
        }

       

        try {
            if($provider_id){
                $Document = ProviderDocument::where('provider_id', $provider_id)
                ->where('document_id', $id)
                ->firstOrFail();
            }
            if($fleet_service_id){
                $Document = ProviderDocument::where('fleet_service_id', $fleet_service_id)
                ->where('document_id', $id)
                ->firstOrFail();
            }
            

            $Document->update([
                    'url' => $request->document->store('provider/documents'),
                    'status' => 'ASSESSING',
                ]);

            return back();

        } catch (ModelNotFoundException $e) {
            ProviderDocument::create([
                    'url' => $request->document->store('provider/documents'),
                    'provider_id' => $provider_id,
                    'document_id' => $id,
                    'fleet_service_id' => $fleet_service_id,
                    'document_type' => $document_type,
                    'status' => 'ASSESSING',
                ]);
            
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
