<?php

namespace App\Http\Controllers\FleetAuth;

use App\Fleet;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Provider;
use App\FleetService;
use App\ProviderService;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/fleet/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('fleet.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:fleets',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Fleet
     */
    protected function create(array $data)
    {
        return Fleet::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('fleet.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('fleet');
    }

    public function create_provider(Request $request,$fleet_id){

  
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email'     => 'unique:providers|email',
                'phone' => 'required|digits_between:6,13',
            ]
        );
        
        if($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->all()]);
        }

        try{
            
            $provider = $request->all();
            $provider['fleet']=$fleet_id;
            $provider['mobile']=$request->phone;
            $provider['password'] = bcrypt($request->password);
            $provider = Provider::create($provider);
            return $provider;
        } catch (Exception $e) {
             return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    public function get_provider(Request $request,$fleet_id){
        
        try{
            $record = Provider::where('fleet', '=', $fleet_id)->get(['id','first_name', 'last_name','email','mobile','rating','status']);
            return $record;
        }catch (Exception $e){
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    public function create_service(Request $request,$fleet_id){
        
        $validator = Validator::make(
            $request->all(),
            [
                'service_type_id' => 'required',
                'service_model' => 'required|max:255',
                'service_number' => 'required|max:255'
            ]
        );
        
        if($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->all()]);
        }

        try{

            $service = $request->all();
            $service['fleet_id'] = $fleet_id;
            $service = FleetService::create($service);
            return $service;

        }catch (Exception $e) {
             return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }

    }

    public function get_service(Request $request,$fleet_id){
        
        try{
            $record = FleetService::where('fleet_id', '=', $fleet_id)->get();
            return $record;
        }catch (Exception $e){
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    public function map_service(Request $request,$fleet_id){

        $validator = Validator::make(
            $request->all(),
            [
                'vehicle_type_id' => 'required',
                'driver_ids' => 'required'
            ]
        );
        
        if($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->all()]);
        }

        $providerservice['fleet_service_id'] = $request->vehicle_type_id;
        
        //validate the fleet_service_id is correct or not for fleet
            $fleetservice = FleetService::where('fleet_id', '=', $fleet_id)
                            ->where('id', '=', $providerservice['fleet_service_id'])
                            ->first();

            if(empty($fleetservice)){
                return response()->json(['error' => 'service is not register with fleet'], 422);
            }

        $providerservice['service_type_id'] = $fleetservice->service_type_id;
        $providerservice['status'] = 'offline';
        $providerservice['service_number'] = $fleetservice->service_number;
        $providerservice['service_model'] = $fleetservice->service_model;


        //validate the service information
            $driverids = array();
            if(!empty($request->driver_ids)){
                foreach($request->driver_ids as $key => $val){
                    $driverids[]=$val['driver_id'];
                }
                $providercnt = Provider::where('fleet', '=', $fleet_id)
                            ->whereIn('id', $driverids)
                            ->count();
                if(count($driverids) != $providercnt){
                    return response()->json(['error' => 'Driver details provided, few not belongs to fleet'], 422);
                }

                $chkonline = ProviderService::whereIn('provider_id', $driverids)->where('status','!=','offline')->count();

                if($chkonline != 0){
                    return response()->json(['error' => 'Few drivers already active/running.Modification not allow.'], 422);
                }

                foreach($driverids as $k=>$v){
                    $FetchProviderService = ProviderService::where('provider_id', $v)
                    ->where('fleet_service_id','!=', 0)
                    ->first();
                    $providerservice['provider_id'] = $v;

                    if(empty($FetchProviderService)){
                        ProviderService::create($providerservice);
                    }else{
                        $FetchProviderService->update($providerservice);
                    }
                }

                return response()->json(['success' => 'Driver and Service mapping done successfully'], 200);
            }else{
                return response()->json(['error' => 'Please provide correct driver details'], 422);
            }
    }

}
