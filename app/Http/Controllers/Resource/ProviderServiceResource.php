<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SendPushNotification;

use App\Provider;
use App\ServiceType;
use App\ProviderService;
use App\ProviderDocument;
use App\FleetService;

class ProviderServiceResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fleet = $request->input('fleet');
        $serviceid = $request->input('serviceid');
        try {
            $Provider = Provider::where('fleet',$fleet)->pluck('first_name','id');

        ///echo '<pre>'; print_r($Provider);exit;
            $FleetService = FleetService::where('id',$serviceid)->with('service_type')->first();
            $ProviderService = ProviderService::where('fleet_service_id',$serviceid)->with('service_type','provider')->get();
            //echo '<pre>';echo $serviceid;
            //print_r($ProviderService);exit;
            $ServiceTypes = ServiceType::all();
            return view('admin.providers.service.index', compact('Provider', 'ServiceTypes','ProviderService','FleetService','fleet','serviceid'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('admin.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $fleet = $request->input('fleet');
        $serviceid = $request->input('serviceid');
        if(!$request->provider_id){
            return redirect()->route('admin.provider.fleetservice.index',['fleet'=>$fleet,'serviceid'=>$serviceid])->with('flash_error', 'Please add provider first');
        }

         try {
            $Document = ProviderService::where('provider_id', $request->provider_id)
                ->where('fleet_service_id', $request->fleet_service_id)
                ->firstOrFail();

            return redirect()->route('admin.provider.fleetservice.index',['fleet'=>$fleet,'serviceid'=>$serviceid])->with('flash_error', 'Fleet service already mapped!');
        } catch (ModelNotFoundException $e) {

            try{
                $Document = ProviderService::where('provider_id', $request->provider_id)
                ->where('fleet_service_id','!=', 0)
                ->firstOrFail();

                if($Document->status!='offline'){
                    return redirect()->route('admin.provider.fleetservice.index',['fleet'=>$fleet,'serviceid'=>$serviceid])->with('flash_error', 'Service currently on running or active state.');
                }else{
                    $Document->update([
                        'provider_id' => $request->provider_id,
                        'service_type_id' => $request->service_type_id,
                        'fleet_service_id' => $request->fleet_service_id,
                        'status' => 'offline',
                        'service_number' => $request->service_number,
                        'service_model' => $request->service_model,
                    ]);
                }

                return redirect()->route('admin.provider.fleetservice.index',['fleet'=>$fleet,'serviceid'=>$serviceid])->with('flash_success', 'Fleet service updated successfully!');
                
            } catch (ModelNotFoundException $e) {
                ProviderService::create([
                    'provider_id' => $request->provider_id,
                    'service_type_id' => $request->service_type_id,
                    'fleet_service_id' => $request->fleet_service_id,
                    'status' => 'offline',
                    'service_number' => $request->service_number,
                    'service_model' => $request->service_model,
                ]);
            }
            
        

        return redirect()->route('admin.provider.fleetservice.index',['fleet'=>$fleet,'serviceid'=>$serviceid])->with('flash_success', 'Fleet service created successfully!');
        }      
        
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($provider, $id)
    {
        try {
            $Document = ProviderDocument::where('provider_id', $provider)
                ->findOrFail($id);

            return view('admin.providers.document.edit', compact('Document'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('admin.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $provider, $id)
    {
        try {

            $Document = ProviderDocument::where('provider_id', $provider)
                ->where('document_id', $id)
                ->firstOrFail();
            $Document->update(['status' => 'ACTIVE']);

            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_success', 'Provider document has been approved.');
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', 'Provider not found!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($provider, $id)
    {
        //echo $provider;exit;
        try {

            $ProviderService = ProviderService::where('provider_id', $provider)
                ->where('id', $id)
                ->firstOrFail();
            $ProviderService->delete();

           // return redirect()
                //->route('admin.provider.document.index', $provider)
               // ->with('flash_success', 'Provider document has been deleted');
               return redirect()->back()->with('flash_success', 'Provider document has been deleted');;
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', 'Provider not found!');
        }
    }

    /**
     * Delete the service type of the provider.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function service_destroy(Request $request, $provider, $id)
    {
        try {

            $FleetService = FleetService::where('fleet_id', $provider)->firstOrFail();
            $FleetService->delete();

            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_success', 'Fleet service has been deleted.');
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', 'Fleet service not found!');
        }
    }
}
