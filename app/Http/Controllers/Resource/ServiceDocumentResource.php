<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SendPushNotification;

use App\Provider;
use App\ServiceType;
use App\Document;
use App\ProviderService;
use App\ProviderDocument;
use App\FleetService;

class ServiceDocumentResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $fleet)
    {
        try {
            $FleetService = FleetService::where('fleet_id',$fleet)->with('service_type')->get();
            $ServiceTypes = ServiceType::all();
            return view('admin.providers.service.create', compact('ServiceTypes','FleetService','fleet'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('admin.index');
        }
    }

    public function proindex(Request $request, $service)
    {
        try {

            $ServiceDocuments = Document::vehicle()->get();
            $FleetService = FleetService::where('id',$service)->with('service_type')->first();
    ///echo '<pre>';print_r($FleetService->documents);exit;
            return view('admin.providers.document.serviceindex', compact('ServiceDocuments','FleetService'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('admin.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $fleet_id)
    { 
        $this->validate($request, [
                'service_type' => 'required|exists:service_types,id',
                'service_number' => 'required',
                'service_model' => 'required',
            ]);
        

        try {
            
            FleetService::create([
                    'fleet_id' => $fleet_id,
                    'service_type_id' => $request->service_type,
                    'service_number' => $request->service_number,
                    'service_model' => $request->service_model,
                ]);

        } catch (ModelNotFoundException $e) {
            
        }
        return back()->with('flash_success', 'Fleet service type updated successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($fleet_service_id, $id)
    {
        try {

            $Document = ProviderDocument::where('fleet_service_id', $fleet_service_id)
                ->findOrFail($id);

                $adapter = \Storage::disk('s3')->getDriver()->getAdapter();       

    $command = $adapter->getClient()->getCommand('GetObject', [
        'Bucket' => $adapter->getBucket(),
        'Key'    => $adapter->getPathPrefix().$Document->url
    ]);

    $request = $adapter->getClient()->createPresignedRequest($command, '+15 minute');
    $Document->url = $request->getUri();
    return view('admin.providers.document.serviceedit', compact('Document','fleet_service_id'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('admin.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $fleet_service_id, $id)
    {
        try {

            $Document = ProviderDocument::where('fleet_service_id', $fleet_service_id)
                ->where('id', $id)
                ->firstOrFail();
            $Document->update(['status' => 'ACTIVE']);

            return redirect()
                ->route('admin.provider.serdocument.index', $fleet_service_id)
                ->with('flash_success', 'Service document has been approved.');
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', 'Service not found!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($fleet_service_id, $id)
    {
        try {

            $Document = ProviderDocument::where('fleet_service_id', $fleet_service_id)
                ->where('id', $id)
                ->firstOrFail();
            $Document->delete();

            return redirect()->route('admin.provider.serdocument.index', $fleet_service_id)->with('flash_success', 'Service document has been deleted');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service not found!');
        }
    }

    /**
     * Delete the service type of the provider.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function service_destroy(Request $request, $provider, $id)
    {
        try {

            $FleetService = FleetService::where('fleet_id', $provider)->firstOrFail();
            $FleetService->delete();

            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_success', 'Fleet service has been deleted.');
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', 'Fleet service not found!');
        }
    }
}
