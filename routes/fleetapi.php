<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication
Route::post('/register' , 'FleetAuth\TokenController@register');
Route::post('/verify' , 'FleetAuth\TokenController@fleetEmailCheck');
Route::post('/oauth/token' , 'FleetAuth\TokenController@authenticate');
Route::post('/otp/token' , 'FleetAuth\TokenController@authenticate_otp');
Route::post('/logout' , 'FleetAuth\TokenController@logout');

Route::post('/auth/facebook', 'FleetAuth\TokenController@facebookViaAPI');
Route::post('/auth/google', 'FleetAuth\TokenController@googleViaAPI');

Route::post('/forgot/password',     'FleetAuth\TokenController@forgot_password');
Route::post('/reset/password',      'FleetAuth\TokenController@reset_password');
Route::post('/otp', 'FleetAuth\TokenController@request_update_otp');


// Add Fleet Driver


Route::group(['middleware' => ['fleet.api']], function () {

Route::post('/refresh/token' , 'FleetAuth\TokenController@refresh_token');

Route::post('/{fleet_id}/provider', 'FleetAuth\RegisterController@create_provider');
Route::post('/{fleet_id}/getprovider', 'FleetAuth\RegisterController@get_provider');

Route::post('/{fleet_id}/service', 'FleetAuth\RegisterController@create_service');
Route::post('/{fleet_id}/getservice', 'FleetAuth\RegisterController@get_service');

Route::post('/{fleet_id}/mapservice', 'FleetAuth\RegisterController@map_service');

 
});